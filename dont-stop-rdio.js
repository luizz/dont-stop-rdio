;(function(doc , w) {
    'use strict';

    var time;
	
	var dontStopRdio = function() {

		clearTimeout(time);
		time = setTimeout(dontStopRdio, 2000);
		
		//Is AdsBlock
        var dialogButton = doc.querySelectorAll( '.Dialog_MessageDialog .Dialog_ButtonBar button[data-cid]'), adModalFull = doc.querySelectorAll('.ad_container .ad_table .ad_image');

        !(adModalFull.length !== 1 && dialogButton.length !== 2) && jQuery.when(w.R.Services.ready("Loader"), w.R.Services.ready("Updater")).done(function(){w.R.loader.load(["Desktop.Main"],function(){w.R.player._playAnAd()})});
	
		dialogButton.length == 2 && dialogButton[1].click();//close
    };
	
    time = setTimeout(dontStopRdio, 3000);

})( document, window );